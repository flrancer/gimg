exports.gImgKwd = function(){
	
	const request = require('request');
	
	this.getImgKwd = function( img_url ){
		return new Promise(resolve => {
		
			var options = {
							url: process.env.gImgHost + img_url,
							headers: {
										'Host': 'images.google.com',
										'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0',
										'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
										'Accept-Language': 'en-US,en;q=0.5',
										'Cache-Control': 'no-cache',
										'Referer': 'https://www.google.com',
									}
						};
			
			request(options, function(error, response, body){
				if( kwd = body.match(/<input[^>]*class=[\'"]gLFyf gsfi[\'"][^>]*value=[\'"]([^\'"]+)[\'"][^>]*>/) )
					resolve( kwd[1] ); 
					
				resolve( false );
			});
			
		})
	}

}